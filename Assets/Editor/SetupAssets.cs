using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public static class EdmSetupAssets
{
    [MenuItem("EDM/Setup Assets")]
    private static void SetupAssets()
    {
        var paths = AssetDatabase.GetAllAssetPaths();

        AssetDatabase.StartAssetEditing();
        foreach (var path in paths)
        {
            var filename = Path.GetFileName(path);

            if (string.Equals(filename, "Google.IOSResolver.dll", StringComparison.OrdinalIgnoreCase))
                AddDefineConstraint(path, "UNITY_IOS");

            if (string.Equals(filename, "Google.JarResolver.dll", StringComparison.OrdinalIgnoreCase))
                AddDefineConstraint(path, "UNITY_ANDROID");
        }
        AssetDatabase.StopAssetEditing();

        Debug.Log("Done!");
    }

    private static void AddDefineConstraint(string path, string directive)
    {
        var importer = (PluginImporter)AssetImporter.GetAtPath(path);
        if (importer.DefineConstraints.Contains(directive)) return;

        var src = importer.DefineConstraints;
        var dst = new string[src.Length + 1];
        Array.Copy(src, dst, src.Length);
        dst[src.Length] = directive;
        importer.DefineConstraints = dst;
        importer.SaveAndReimport();
    }
}
