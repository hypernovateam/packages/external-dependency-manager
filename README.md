# External Dependency Manager for Unity

github: <https://github.com/googlesamples/unity-jar-resolver>

Changes:
- Google.IOSResolver.dll.meta: added define constraint UNITY_IOS
- Google.JarResolver.dll.meta: added define constraint UNITY_ANDROID
